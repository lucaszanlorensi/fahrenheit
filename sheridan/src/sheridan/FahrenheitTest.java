package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

//lucas Zanlorensi
	
	@Test
	public void fromCelsiusRegularTest(){
		int result = Fahrenheit.fromCelsius(100);
		assertTrue("Wrong conversion", result == 212);
	}
	
	@Test
	(expected=IllegalArgumentException.class)
	public void fromCelsiusExceptionalTest(){
		Fahrenheit.fromCelsius(-100000000);
	}
	
	@Test
	public void fromCelsiusBoundaryInTest(){
		int result = Fahrenheit.fromCelsius(1);
		assertTrue("Wrong conversion", result == 34);
	}
	
	@Test
	public void fromCelsiusBoundaryOutTest(){
		int result = Fahrenheit.fromCelsius(-1);
		System.out.println(result);
		assertTrue("Wrong conversion", result == 30);
	}

}
