package sheridan;

import java.lang.Math;

public class Fahrenheit {
	
	public static int fromCelsius(int celcius) throws IllegalArgumentException{
		
		double  fahrenheit = ( (celcius*9.0)/5.0)+32.0;
		if(fahrenheit < -459.67) throw new IllegalArgumentException("Value too low");
		return (int) Math.round(fahrenheit);
	}

}
